from datetime import datetime
from django.http import HttpResponse, HttpResponseNotFound
from django.views import View
from django.shortcuts import render, redirect
from blog.models import ArticleModel
from django.utils import timezone
from blog.forms import ArticleForm


class Articles(View):
    def get(self, request):
        articles = ArticleModel.objects.all()
        return render(request, "articles.html", {"articles": articles, "form": ArticleForm()})

    def post(self, request):
        form = ArticleForm(request.POST)
        form.instance.created_at = datetime.now(tz=timezone.utc)
        form.save()
        return redirect("/blog/Articles")


class Home(View):
    def get(self, request):
        return HttpResponse("GET request")

    def post(self, request):
        return HttpResponse("Post request")


class ArticleDetail(View):
    def get(self, request, id):
        try:
            article = ArticleModel.objects.get(id=id)
            return render(request, "article_details.html", {"article": article})
        except ArticleModel.DoesNotExist:
            return HttpResponseNotFound()
